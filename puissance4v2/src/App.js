import * as React from "react";
import "./App.css";

/***
 J'ai oublié les cours de react du coup je me suis aidé de ça http://defeo.lu/aws/tutorials/tutorial2 et sur ça https://www.youtube.com/watch?v=HCgu4F4qlmg
**/

class Puissance4 {
  
    //On initialise un tableau 
    //et fait l'affichage dans l'élément `element_id` du DOM.
  constructor(element_id, rows=6, cols=7) {
    // On initialise le nombre de ligne et de colonne
    this.rows = rows;
    this.cols = cols;
  	// On fait un tableau on dit que le 0 = case vide du coup on peut bouger
    // On dit que le 1 = le pion du joueur 1
    // On dit que le 2 = Le pion du joueur 2
    this.plateau = Array(this.rows);
    for (let i = 0; i < this.rows; i++) {
      this.plateau[i] = Array(this.cols).fill(0);
    }
    // Le tour change en fonction de si c'est 1 ou 2
    this.turn = 1;
    // Nombre de coups joués
    this.moves = 0;
    /* un entier indiquant le gagnant:
        null: la partie continue
        Si c'est 0: il y a egalité 
        Si c'est 1: le joueur 1 a gagné
        Si c'est 2: le joueur 2 a gagné
    */
    this.winner = null;

    // L'élément du DOM où se fait l'affichage
    this.element = document.querySelector(element_id);
    this.element.addEventListener('click', (event) => this.handle_click(event));
    // On fait l'affichage avec render dans le DOM
    this.render();
  }
  
  /* Affiche le plateau de jeu dans le DOM avec render*/
  render() {
    let table = document.createElement('table');
    for (let i = this.rows - 1; i >= 0; i--) {
      let tr = table.appendChild(document.createElement('tr'));
      for (let j = 0; j < this.cols; j++) {
        let td = tr.appendChild(document.createElement('td'));
        let colour = this.plateau[i][j];
        if (colour)
          td.className = 'player' + colour;
        td.dataset.column = j;
      }
    }
    this.element.innerHTML = '';
    this.element.appendChild(table);
  }
  /* Cette fonction ajoute un pion dans une colonne */
	play(column) {
    // Trouver la première case libre dans la colonne
    let row;
    for (let i = 0; i < this.rows; i++) {
      if (this.plateau[i][column] == 0) {
        row = i;
        break;
      }
    }
    if (row === undefined) {
      return null;
    } else {
      // Effectuer le coup
      this.set(row, column, this.turn);
      // Renvoyer la ligne où on a joué
      return row;
    }
	}
  
  
	set(row, column, player) {
    // On colore la case
	  this.plateau[row][column] = player;
    // On compte le coup
    this.moves++;
	}

  
  handle_click(event) {

	  let column = event.target.dataset.column;
  	if (column !== undefined) {
      //On parseInt pour evité des problemes 
      column = parseInt(column);
     	let row = this.play(parseInt(column));
      
      if (row === null) {
        window.alert("Il n'y a plus de place");
      } else {
        // Vérifier s'il y a un gagnant, ou si la partie est finie
        if (this.win(row, column, this.turn)) {
          this.winner = this.turn;
        } else if (this.moves >= this.rows * this.columns) {
          this.winner = 0;
        }
        // Passer le tour : 3 - 2 = 1, 3 - 1 = 2
        this.turn = 3 - this.turn;

        // Mettre à jour l'affichage
        this.render()
        
        // On switch les messages de fin de partie
        switch (this.winner) {
          case 0: 
            window.alert("Egalité"); 
            break;
          case 1:
            window.alert("Les Rouges gagnent"); 
            break;
          case 2:
            window.alert("Les jaunes gagnent"); 
            break;
        }
      }
    }
  }

  /* 
   Cette fonction vérifie si le coup dans la case `row`, `column` par
   le joueur `player` est un coup gagnant.
   
   Renvoie :
     true  : si la partie est gagnée par le joueur `player`
     false : si la partie continue
 */
	win(row, column, player) {
		// Horizontal
    let count = 0;
    for (let j = 0; j < this.cols; j++) {
      count = (this.plateau[row][j] == player) ? count+1 : 0;
      if (count >= 4) return true;
    }
		// Vertical
    count = 0;
    for (let i = 0; i < this.rows; i++) {
      count = (this.plateau[i][column] == player) ? count+1 : 0;
	    if (count >= 4) return true;
    }
		// Diagonal
    count = 0;
    let shift = row - column;
    for (let i = Math.max(shift, 0); i < Math.min(this.rows, this.cols + shift); i++) {
      count = (this.plateau[i][i - shift] == player) ? count+1 : 0;
    	if (count >= 4) return true;
    }
		// Anti-diagonal
    count = 0;
    shift = row + column;
    for (let i = Math.max(shift - this.cols + 1, 0); i < Math.min(this.rows, shift + 1); i++) {
      console.log(i,shift-i,shift)
      count = (this.plateau[i][shift - i] == player) ? count+1 : 0;
      if (count >= 4) return true;
    }
    
    return false;
	}
}

// On initialise le plateau et on visualise dans le DOM
// (dans la balise d'identifiant `game`).
let game = new Puissance4('#game');
export default App;

